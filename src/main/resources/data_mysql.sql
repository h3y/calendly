USE `calendly_db`;

CREATE TABLE IF NOT EXISTS `calendly_user` (
     `id` int(11) NOT NULL,
     `create_at` bigint(20) DEFAULT NULL,
     `email` varchar(255) DEFAULT NULL,
     `name` varchar(255) DEFAULT NULL,
     `timezone` varchar(255) DEFAULT NULL,
     `update_at` bigint(20) DEFAULT NULL,
     `user_id` varchar(255) DEFAULT NULL,
     `user_name` varchar(255) DEFAULT NULL,
     `created_at` bigint(20) DEFAULT NULL,
     `updated_at` bigint(20) DEFAULT NULL,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `calendly_event` (
    `id` int(11) NOT NULL,
    `active` bit(1) DEFAULT NULL,
    `create_at` bigint(20) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    `duration` int(11) DEFAULT NULL,
    `event_id` varchar(255) DEFAULT NULL,
    `location` varchar(255) DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    `update_at` bigint(20) DEFAULT NULL,
    `url` varchar(255) DEFAULT NULL,
    `user_id` int(11) NOT NULL,
    `created_at` bigint(20) DEFAULT NULL,
    `updated_at` bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKcmuh34hn46bnab51sfx35w3m8` (`user_id`),
    CONSTRAINT `FKcmuh34hn46bnab51sfx35w3m8` FOREIGN KEY (`user_id`) REFERENCES `calendly_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;