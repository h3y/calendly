package com.example.calendly.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
@ConfigurationProperties("calendly")
public class CalendlyConfig {
    private String baseUrl;
    private String apiKey;
    private String eventsUrl;
    private String myProfileUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEventsUrl() {
        return eventsUrl;
    }

    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    public String getMyProfileUrl() {
        return myProfileUrl;
    }

    public void setMyProfileUrl(String myProfileUrl) {
        this.myProfileUrl = myProfileUrl;
    }
}
