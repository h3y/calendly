package com.example.calendly.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalendlyUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String userId;

    private String userName;

    private String name;

    private String email;

    private String timezone;

    private Long createdAt;

    private Long updatedAt;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<CalendlyEvent> events;
}
