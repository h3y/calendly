package com.example.calendly.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalendlyEvent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String eventId;

    private String name;

    private String description;

    private String url;

    private Integer duration;

    private String location;

    private Long createdAt;

    private Long updatedAt;

    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private CalendlyUser user;

}
