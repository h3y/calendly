package com.example.calendly.controller;

import com.example.calendly.helper.ResponseModel;
import com.example.calendly.model.CalendlyEvent;
import com.example.calendly.service.CalendlyService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Calendly Integration Controller")
@RestController
@RequestMapping(value = "/calendly")
public class CalendlyController {

    @Autowired
    CalendlyService calendlyService;

    @ApiOperation(value = "Pull events from Calendly")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully pulled list"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    @ResponseBody
    public List<CalendlyEvent> refreshEvents() {
        return calendlyService.pullEvents();
    }

    @ApiOperation(value = "View a list of available events")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<CalendlyEvent> list(
            @ApiParam(name = "name", value = "user name") @RequestParam(value = "name", required = false) String name,
            @ApiParam(name = "userId", value = "user id") @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(name = "active", value = "Active task") @RequestParam(value = "active", required = false) Boolean active, Pageable pageable) {
        return calendlyService.getEvents(name,userId, active, pageable);
    }

}
