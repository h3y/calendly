package com.example.calendly.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ResponseModel implements Serializable {

    public ResponseModel(Object value) {
        this.response = value;
    }

    private Object response;


    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }
}
