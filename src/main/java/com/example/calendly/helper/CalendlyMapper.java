package com.example.calendly.helper;

import com.example.calendly.model.CalendlyEvent;
import com.example.calendly.model.CalendlyUser;
import com.example.calendly.viewModel.CalendlyEventsViewModel;
import com.example.calendly.viewModel.CalendlyUserViewModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CalendlyMapper {
    public CalendlyUser toUserModel(CalendlyUserViewModel userViewModel) {
        return CalendlyUser.builder()
                .userId(userViewModel.getData().getId())
                .email(userViewModel.getData().getAttributes().getEmail())
                .createdAt(userViewModel.getData().getAttributes().getCreatedAt().getTimeInMillis())
                .updatedAt(userViewModel.getData().getAttributes().getUpdatedAt().getTimeInMillis())
                .name(userViewModel.getData().getAttributes().getName())
                .userName(userViewModel.getData().getAttributes().getSlug())
                .timezone(userViewModel.getData().getAttributes().getTimezone())
                .build();
    }

    public List<CalendlyEvent> toEventsModel(CalendlyEventsViewModel eventsViewModel, CalendlyUser user) {
        List<CalendlyEvent> calendlyEvents = new ArrayList<>();
        for (CalendlyEventsViewModel.Data data : eventsViewModel.getData()) {
            CalendlyEvent event = CalendlyEvent.builder()
                    .eventId(data.getId())
                    .name(data.getAttributes().getName())
                    .description(data.getAttributes().getDescription())
                    .url(data.getAttributes().getUrl())
                    .duration(data.getAttributes().getDuration())
                    .location(data.getAttributes().getLocation())
                    .createdAt(data.getAttributes().getCreatedAt().getTimeInMillis())
                    .updatedAt(data.getAttributes().getUpdatedAt().getTimeInMillis())
                    .active(data.getAttributes().getActive())
                    .user(user)
                    .build();
            calendlyEvents.add(event);
        }
        return calendlyEvents;
    }
}
