package com.example.calendly.viewModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;

@Getter
@Setter
public class CalendlyUserViewModel {
    private Data data;

    @Getter
    @Setter
    public static class Data {
        private String id;
        private Attributes attributes;
        @Getter
        @Setter
        public static class Attributes {
            private String slug;
            private String name;
            private String email;
            private String timezone;
            @JsonProperty("created_at")
            private Calendar createdAt;
            @JsonProperty("updated_at")
            private Calendar updatedAt;
        }
    }
}
