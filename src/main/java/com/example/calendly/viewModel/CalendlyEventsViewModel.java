package com.example.calendly.viewModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;
import java.util.List;

@Getter
@Setter
public class CalendlyEventsViewModel {

    private List<Data> data;
    @Getter
    @Setter
    public static class Data {
        private String id;
        private Attributes attributes;

        @Getter
        @Setter
        public class Attributes {
            private String name;
            private String description;
            private String url;
            private Integer duration;
            private String location;
            @JsonProperty("created_at")
            private Calendar createdAt;
            @JsonProperty("updated_at")
            private Calendar updatedAt;
            private Boolean active;
        }
    }

}


