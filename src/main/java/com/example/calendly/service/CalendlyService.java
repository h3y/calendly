package com.example.calendly.service;

import com.example.calendly.config.CalendlyConfig;
import com.example.calendly.dao.ICalendlyEventDao;
import com.example.calendly.dao.ICalendlyUserDao;
import com.example.calendly.helper.CalendlyMapper;
import com.example.calendly.model.CalendlyEvent;
import com.example.calendly.model.CalendlyUser;
import com.example.calendly.viewModel.CalendlyEventsViewModel;
import com.example.calendly.viewModel.CalendlyUserViewModel;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class CalendlyService {

    @Autowired
    private CalendlyConfig calendlyConfig;
    @Autowired
    private ICalendlyUserDao calendlyUserDao;
    @Autowired
    private ICalendlyEventDao calendlyEventDao;
    @Autowired
    private CalendlyMapper calendlyMapper;

    public CalendlyUser getUserFromCalendly() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity entity = new HttpEntity(getHeaders());
        String url = calendlyConfig.getBaseUrl() + calendlyConfig.getMyProfileUrl();
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.GET, entity, CalendlyUserViewModel.class);

        CalendlyUser calendlyUser = calendlyMapper.toUserModel((CalendlyUserViewModel) Objects.requireNonNull(response.getBody()));

        return  AddOrUpdateDbUser(calendlyUser);
    }

    public List<CalendlyEvent> pullEvents() {
        return getEventsFromCalendly();
    }

    public Page<CalendlyEvent> getEvents(String name, String userId, Boolean active, Pageable pageable){
        return calendlyEventDao.findAll(name,userId,active,pageable);
    }

    private List<CalendlyEvent> getEventsFromCalendly() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity entity = new HttpEntity(getHeaders());
        String url = calendlyConfig.getBaseUrl() + calendlyConfig.getEventsUrl();
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.GET, entity, CalendlyEventsViewModel.class);

        List<CalendlyEvent> calendlyEvents = calendlyMapper.toEventsModel((CalendlyEventsViewModel) Objects.requireNonNull(response.getBody()), getUserFromCalendly());

        return AddOrUpdateDbEvents(calendlyEvents);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-TOKEN", calendlyConfig.getApiKey());
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }


    private CalendlyUser AddOrUpdateDbUser(CalendlyUser user) {
        CalendlyUser dbUser = calendlyUserDao.findByUserId(user.getUserId());
        if (dbUser != null) {
            user.setId(dbUser.getId());
        }
        return calendlyUserDao.save(user);
    }

    private List<CalendlyEvent> AddOrUpdateDbEvents(List<CalendlyEvent> events) {
        List<CalendlyEvent> calendlyEvents = new ArrayList<>();
        for (CalendlyEvent event : events) {
            CalendlyEvent dbEvent = calendlyEventDao.findByEventId(event.getEventId());
            if (dbEvent != null) {
                event.setId(dbEvent.getId());
            }
            calendlyEvents.add(event);
        }
        return  Lists.newArrayList(calendlyEventDao.saveAll(calendlyEvents));
    }


}
