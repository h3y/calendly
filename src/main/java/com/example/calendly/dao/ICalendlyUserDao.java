package com.example.calendly.dao;

import com.example.calendly.model.CalendlyUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ICalendlyUserDao extends PagingAndSortingRepository<CalendlyUser, Integer> {

    @Query(value = "select u from CalendlyUser u where u.userId =:userId")
    CalendlyUser findByUserId(@Param("userId") String userId);
}
