package com.example.calendly.dao;

import com.example.calendly.model.CalendlyEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ICalendlyEventDao extends PagingAndSortingRepository<CalendlyEvent, Integer> {
    @Query(value = "select e from CalendlyEvent e where e.eventId =:eventId")
    CalendlyEvent findByEventId(@Param("eventId") String eventId);

    @Query(value = "select e from CalendlyEvent e left join e.user u where " +
            "(:userId is null or u.userId = :userId) and " +
            "(:name is null or u.userName LIKE CONCAT('%',:name,'%')) and " +
            "(:active is null or e.active = :active)")
    Page<CalendlyEvent> findAll(@Param("name") String name, @Param("userId") String userId, @Param("active") Boolean active, Pageable var1);
}
