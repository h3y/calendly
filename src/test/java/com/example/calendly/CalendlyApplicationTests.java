package com.example.calendly;

import com.example.calendly.model.CalendlyEvent;
import com.example.calendly.service.CalendlyService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class CalendlyApplicationTests {

	@Autowired
	private CalendlyService calendlyService;

	@Test
	void UserExist(){
		assertNotNull(calendlyService.getUserFromCalendly());
	}

	@Test
	void testWithRetriveDataFromCalendly(){
		String userId = calendlyService.pullEvents().get(0).getUser().getUserId();
		assertEquals("DBHAA56EYY6TU74C",userId);
	}

	@Test
	void testWithContainsEvents(){
		List<CalendlyEvent> eventsPull = calendlyService.pullEvents();
		assertNotNull(eventsPull);
		List<CalendlyEvent> dbEvent = calendlyService.getEvents(null,null,null,Pageable.unpaged()).getContent();
		assertNotNull(dbEvent);
		assertEquals(eventsPull.size(),dbEvent.size());
	}

	@Test
	void testWithContainsNonActiveEvents(){
		List<CalendlyEvent> dbEvent = calendlyService.getEvents(null,null,false,Pageable.unpaged()).getContent();
		assertNotNull(dbEvent);
		assertEquals(0,dbEvent.size());
	}



}
